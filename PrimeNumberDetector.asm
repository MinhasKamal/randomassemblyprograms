##Writer: Minhas Kamal
##Date: 23-Apr-2014
##Function:	Can tell if a number is prime or not


#####**data**#####
.data

prompt: 		.asciiz "\n\nEnter a number(0 to exit): "
is_prime: 		.asciiz "It is a prime number."
is_not_prime: 	.asciiz "It is not a prime number."
exit_message: 	.asciiz "\n\n#Program exits..."


#####**text**#####
.text

main:
loop_1:
	la $a0, prompt			#prompt for user input
	li $v0, 4
	syscall
	
	li $v0, 5				#take integer input 
	syscall
	add $t1, $v0, $zero	
	
	ble $t1, $zero, exit	#terminate when <=0 is inputted 
	beq $t1, 2, isPrime		#when 2 is inputted 
	beq $t1, 3, isPrime		#when 3 is inputted 
	
	li $t0, 2
	div $t4, $t1, $t0		#see if divisible by 2 or not
	mul $t4, $t4, $t0
	beq $t4, $t1, isNotPrime
	
	li $t0, 3
loop_2:
	div $t4, $t1, $t0		#see if divisible or not
	mul $t4, $t4, $t0
	beq $t4, $t1, isNotPrime
	
	addi $t0, $t0, 2		#continue the loop
	blt $t0, $t1, loop_2
	
isPrime:
	la $a0, is_prime
	li $v0, 4
	syscall
	j loop_1
	
isNotPrime:
	la $a0, is_not_prime
	li $v0, 4
	syscall
	j loop_1
	
exit:
	la $a0, exit_message
	li $v0, 4
	syscall
	li $v0, 10
	syscall